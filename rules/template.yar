rule Template : Tracker
{
  meta:
    author = "QuidsUp"
    date = "2021-09-21"
    version = 1
    weight = 1
    description = "Identify A Tracker"
    reference = "The company website"
    source1 = "https://somedomain.com/trackerscript.js"
    source2 = "https://anotherdomain.com/trackerscript.js"

  strings:
    $foo = "bar"

  condition:
    all of them
}
