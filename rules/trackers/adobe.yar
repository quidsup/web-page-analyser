rule Adobe : Tracker
{
  meta:
    author = "QuidsUp"
    date = "2021-09-21"
    version = 2
    weight = 3
    description = "Identify Adobe JavaScript"
    reference = "https://business.adobe.com/uk/products/analytics/adobe-analytics.html"
    source1 = "https://www.redhat.com/ma/dpal.js"
    source2 = "https://assets.adobedtm.com/d8c1ad59c628/d4f96cd8569f/launch-8b94ebabd8d2.min.js"

  strings:
    $header = "https://assets.adobedtm.com"
    $str1 = "com.adobe.reactor"
    $str2 = "adobe.OptInCategories"
    $str3 = "trackingServerSecure"
    //Primary location of tracking server is stored as a string in trackingServerSecure
    $url1 = /trackingServerSecure:"[\w\-\.]{6,254}/
    //Although some sites like michaelkors.* use an array of alternate domains to parse to trackingServerSecure
    //Unreliable method of identifying domain using the common smetrics / securemetrics subdomain
    $url2 = /"s(ecure)?metrics\.[\w\-\.]{6,254}/

  condition:
    $header in (30..50) and all of ($str*) and any of ($url*)
}
