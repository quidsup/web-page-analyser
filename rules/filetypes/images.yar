//https://en.wikipedia.org/wiki/List_of_file_signatures

rule GIF : Image
{
  meta:
    author = "QuidsUp"
    date = "2021-09-20"
    version = 1
    description = "Identify GIF Header dropping 87/89 version for simplicity"
    reference = "GIF8 = 47 49 46 38"

  condition:
    uint32(0) == 0x38464947
}

rule JPEG : Image
{
  meta:
    author = "QuidsUp"
    date = "2021-09-20"
    version = 1
    description = "Identify JPEG Header"
    reference = "ff d8 ff (db | e0 | e1 | ee)"

  condition:
    uint16(0) == 0xd8ff and uint8(2) == 0xff
}

rule PNG : Image
{
  meta:
    author = "QuidsUp"
    date = "2021-09-20"
    version = 1
    description = "Identify PNG Header"
    reference = "x89PNG = 89 50 4e 47"

  condition:
    uint32(0) == 0x474e5089
}

rule SVG : Image
{
  meta:
    author = "QuidsUp"
    date = "2021-09-20"
    version = 1
    description = "Identify SVG Header"
    reference = "<svg = 3c 73 76 67"

  condition:
    uint32(0) == 0x6776733c
}

rule WebP : Image
{
  meta:
    author = "QuidsUp"
    date = "2021-09-20"
    version = 1
    description = "Identify WebP Header"
    reference = "52 49 46 46 ?? ?? ?? ?? 57 45 42 50"

  condition:
    uint32(0) == 0x46464952 and uint32(8) == 0x50424557
}
