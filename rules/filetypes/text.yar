//Text file types
rule JavaScript : Text
{
  meta:
    author = "QuidsUp"
    date = "2021-09-19"
    version = 1
    description = "Identify JavaScript file in simple crude way"
    reference = ""
    // / * = 2f 2a

  condition:
    uint16(0) == 0x2a2f
}

rule HTML : Text
{
  meta:
    author = "QuidsUp"
    date = "2021-09-19"
    version = 1
    description = "Identify a HTML file using the <html tag"
    reference = "https://html.spec.whatwg.org/#the-html-element"

  strings:
    $header = "<html" nocase

  condition:
    $header in (0..50)
}
