rule Beacon : Generic
{
  meta:
    author = "QuidsUp"
    date = "2021-09-21"
    version = 1
    weight = 1
    description = "Generic key words for trackers"
    reference = ""

  strings:
    $beacon = "Beacon"

  condition:
    #beacon > 7
}
