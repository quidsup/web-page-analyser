rule Screen : Fingerprint
{
  meta:
    author = "QuidsUp"
    date = "2021-09-21"
    version = 1
    weight = 1
    description = "Basic Fingerprint"
    reference = "https://github.com/yut/feedback/blob/master/examples.js"

  strings:
    $str1 = ".height"
    $str2 = ".width"
    $str3 = ".devicePixelRatio"
    $str4 = ".location.host"
    $str5 = ".location.pathname"

  condition:
    all of them
}
