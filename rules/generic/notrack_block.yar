rule NoTrack : Blockpage
{
  meta:
    author = "QuidsUp"
    date = "2021-09-24"
    version = 1
    weight = 10
    description = "Identify NoTrack block page"
    reference = "https://gitlab.com/quidsup/notrack/-/tree/master/sink"

  strings:
    $title = "<title>Blocked by NoTrack</title>"

  condition:
    //<!DO CTYP and title
    uint32(0) == 0x4f44213c and uint32(4) == 0x50595443 and $title
}
