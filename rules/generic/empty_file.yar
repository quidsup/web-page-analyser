rule EmptyFile : Misc
{
  meta:
    author = "QuidsUp"
    date = "2021-09-25"
    version = 1
    weight = 1
    description = "Identify an empty file"
    reference = "The company website"

  condition:
    filesize == 0
}
