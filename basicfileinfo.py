#!/usr/bin/env python3
#Title       :
#Description :
#Author      : QuidsUp
#Date        : 2021-09-08
#Version     : 0.1
#Usage       : N/A this module is loaded elsewhere

#Standard Imports
from datetime import datetime
from hashlib import sha256
from pathlib import Path

class FileInfo:
    def __init__(self):
        self._created_time = datetime.now()                #Record time object was created (used for download time)
        self._parsed_url = object

        self._file_ext = ''
        self._file_name = ''
        self._sha256hash = ''
        self._binarydata = b''
        self._textdata = ''

        self.categories = list()                           #List of tuples to allow for multiple categories
        self.status_code = 0
        self.datatype = ''
        self.result = ''
        self.url = ''

    def __str__(self):
        download_time = self._created_time.strftime('%H:%M:%S')
        return f'FileInfo({download_time} status_code={self.status_code}, result=\'{self.result}\', datatype=\'{self.datatype}\')'

    @property
    def binarydata(self):
        return self._binarydata

    @binarydata.setter
    def binarydata(self, value):
        """Record the SHA256 Hash of the binary data"""
        self._binarydata = value
        self._sha256hash = sha256(value).hexdigest()

    @property
    def textdata(self):
        return self._textdata

    @textdata.setter
    def textdata(self, value):
        self._textdata = value

    @property
    def created_time(self):
        return self._created_time

    @property
    def parsed_url(self):
        return self._parsed_url

    @parsed_url.setter
    def parsed_url(self, value):
        """
        Extract file name and file path at from the urllib.parse object
        """
        p = Path(value.path.lower())                       #Extract file name from path
        self._file_name = p.name                           #Set lowercase file name
        self._file_ext = p.suffix                          #Set lowercase extension
        self._parsed_url = value

    @property
    def file_ext(self):
        return self._file_ext

    @property
    def file_name(self):
        return self._file_name

    @property
    def sha256hash(self):
        return self._sha256hash
