#!/usr/bin/env python3
#Title       :
#Description :
#Author      : QuidsUp
#Date        : 2021-09-08
#Version     : 0.1
#Usage       : N/A this module is loaded elsewhere


#Standard Imports
import argparse
import configparser
import os

class WebAnalyserConfig:
    def __init__(self, require_config_file = False):
        WebAnalyserConfig._require_config_file = require_config_file
        WebAnalyserConfig._webpages = list()

        WebAnalyserConfig.validate_ssl = False

    @property
    def webpages(self):
        return WebAnalyserConfig._webpages


    def __get_config_paths(self) -> list:
        """
        Get a list of config locations
        NOTE Set program_name and file_name
        """
        config_paths = list()
        config_home = ''
        program_name = 'webpage_analyser'
        file_name = 'config.ini'

        #Add etc folder for Unix systems
        if os.name == 'posix':
            config_paths.append(os.path.join('/etc', program_name, file_name))

        #Add home folder location: APPDATA for Windows, XDG_CONFIG_HOME for Unix
        if 'APPDATA' in os.environ:
            config_home = os.environ['APPDATA']
        elif 'XDG_CONFIG_HOME' in os.environ:
            config_home = os.environ['XDG_CONFIG_HOME']
        else:
            config_home = os.path.join(os.environ['HOME'], '.config')

        config_paths.append(os.path.join(config_home, program_name, file_name))

        #config_paths.append(os.path.join(os.path.dirname(__file__), file_name))
        return config_paths


    def load(self) -> bool:
        """
        """
        config_paths = list()

        config_paths = self.__get_config_paths()

        cfg = configparser.ConfigParser()
        cfg.read(config_paths)


    def process_arguments(self, argv):
        """
        """
        parser = argparse.ArgumentParser(description = 'Something')

        parser.add_argument("webpage", nargs='+', help='Web page to analyse')
        args = parser.parse_args()

        WebAnalyserConfig._webpages = args.webpage


