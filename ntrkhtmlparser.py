#!/usr/bin/env python3
#Title       :
#Description :
#Author      : QuidsUp
#Date        : 2021-09-16
#Version     : 0.1
#Usage       : N/A this module is loaded elsewhere

#Standard Imports
from html.parser import HTMLParser
from urllib.parse import urljoin

#Local Imports
import errorlogger

#Create logger
logger = errorlogger.logging.getLogger(__name__)

#Custom HTML Parser using Pythons HTMLParser as a parent class
#This class allows implementation of the parent handle_tag functions
#We are only looking for <script> and <img> tags
class NTHTMLParser(HTMLParser):
    def __init__(self, originalurl):
        """self.elements = dict({
            'title' : '',
            'h1' : list(),
        })
        """
        self.remoteimgs = list()
        self.remotescripts = list()
        self.tag_stack = list()

        self.originalurl = ''
        self.originalurl = originalurl

        super().__init__()
        #self.reset()


    def __add_script(self, attrs: list) -> bool:
        """
        Look for src from supplied attributes
        """
        src = list()

        #Search for position then extract src from attrs list
        src = list(filter(lambda x: x[0] == 'src', attrs))

        #Has an src parameter been found?
        if len(src) == 1:
            self.remotescripts.append(self.__unshorten_url(src[0][1]))
            #print('adding src', self.__unshorten_url(src[0][1]))
        else:
            #print('not found', attrs)
            return False

        return True


    def __add_image(self, attrs: list) -> bool:
        """
        Look for datasrc and src from supplied attributes
        """
        datasrc = list()
        src = list()

        #Search for position then extract datasrc and src from attrs list
        datasrc = list(filter(lambda x: x[0] == 'data-src', attrs))
        src = list(filter(lambda x: x[0] == 'src', attrs))

        #Has a datasrc parameter been found?
        if len(datasrc) == 1:
            self.remoteimgs.append(self.__unshorten_url(datasrc[0][1]))
            #print('adding datasrc', self.__unshorten_url(datasrc[0][1]))
        #Has an src parameter been found?
        elif len(src) == 1:
            self.remoteimgs.append(self.__unshorten_url(src[0][1]))
            #print('adding src', self.__unshorten_url(src[0][1]))
        else:
            #print('not found', attrs)
            return False

        return True


    def __unshorten_url(self, targeturl: str):
        """
        HTML URLs can be shortened, relative, or specific
        This function can "unshorten" a shortened or relative URL and return its full location

        Parameters:
            targeturl (str): URL extracted from any HTML element

        Returns:
            unsortened URL - its full location
        """
        longurl = ''

        #Is the targeturl missing https at the beginning?
        if targeturl.startswith('//', 0):
            longurl = f'https:{targeturl}'

        #Is the targeturl a relative path?
        #Relative folders are merged with the original URL using urllib.parse.urljoin
        elif targeturl.startswith('.', 0) or targeturl.startswith('/', 0):
            longurl = urljoin(self.originalurl, targeturl)

        #targeturl is already a long URL
        else:
            return targeturl

        return longurl


    def handle_startendtag(self, tag, attrs):
        #print(f'Encountered a startend tag: {tag}, {attrs}')
        #TODO reserved for link tags
        pass


    def handle_starttag(self, tag, attrs):
        #    print(f'Encountered a start tag: {tag}, {attrs}')
        if tag == 'img':
            self.__add_image(attrs)
        elif tag == 'script':
            self.__add_script(attrs)
        #if tag in self.elements:
        #    self.tag_stack.append(tag)


    def handle_endtag(self, tag):
        #if tag in self.elements:
        #    self.tag_stack.pop()
        #print(f'Encountered an end tag: {tag}')
        pass

    def handle_data(self, data):
        #print(f'Encountered a data tag: {data}')
        """
        if len(self.tag_stack) == 0:
            return

        latest_tag = ''
        latest_tag = self.tag_stack[-1]

        if latest_tag == 'title':
            self.__add_title(data)
        elif latest_tag in self.elements:
            self.elements[latest_tag].append(data)
        """
        pass


    def handle_comment(self, data):
        #print(f'Encountered a comment: {data}')
        pass

    def unknown_decl(self, data):
        #print(f'Encountered unrecognized declaration: {data}')
        pass
