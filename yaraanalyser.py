#!/usr/bin/env python3
#Title       : Yara File Analyser
#Description :
#Author      : QuidsUp
#Date        : 2021-09-19
#Version     : 0.2
#Usage       : yaraanalyser.py file [file ...]

#Standard Imports
import re
try:
    import yara
except ModuleNotFoundError:
    print('Error: Missing yara module')
    print('Install with pip3 install yara-python')
    exit()

#Local Imports
import errorlogger

#Create logger
logger = errorlogger.logging.getLogger(__name__)

#yara.set_config(max_strings_per_rule=50000, stack_size=65536)
rules = yara.compile(filepath='index.yar')

#Regex to extract a quoted domain in a JavaScript Key-Value pair
#TODO perhaps this should also include the parameter arguments?
domainextractor = re.compile(r'[\"\']([\w\.]{4,253})')

class YaraCategories:
    def __init__(self, yaramatches: list):
        """

        """
        self.categories = list()
        self.urls = set()
        category = ''
        tag = ''
        weight = 0

        for item in yaramatches:
            category = ''
            tag = ''
            weight = 0

            category = item.rule
            weight = item.meta.get('weight', 0)

            if len(item.tags) > 0:
                tag = ",".join(item.tags)

            self.categories.append(tuple((category, tag, weight)))
            self.__extract_urls(item.strings)


    def __str__(self):
        categories_str = '';
        urls_str = ''

        if len(self.categories) == 0:
            categories_str = 'None'

        for item in self.categories:
            if item[1] != '':
                categories_str += f'{item[0]} - {item[1]}, {item[2]}\n'
            else:
                categories_str += f'{item[0]}, {item[2]}\n'

        if len(self.urls) == 0:
            urls_str = 'None'
        else:
            urls_str = ", ".join(self.urls)

        return f'Rules Matched:\n{categories_str}\nURLs Found: {urls_str}'


    def __extract_urls(self, strlist: list) -> None:
        """
        Extract url from $url(x) Yara string matches
        strlist is a list of tuples containing:
            0. Byte position found
            1. Varible
            2. String found

        Parameters:
            Yara match.strings list
        """
        urlvalue = ''

        for item in strlist:
            if item[1].startswith('$url'):                 #Look for $url(x)
                #String found can either be a bytes array or ascii string
                if type(item[2]) == bytes:                 #Convert bytes array to string
                    urlvalue = item[2].decode()
                else:                                      #Or variable is already a string
                    urlvalue = item[2]

                self.urls.add(domainextractor.search(urlvalue)[1])


def matchfile(filename: str):
    """
    """
    matches = rules.match(filename)
    if len(matches) == 0:
        print('No results found')
        return None

    #Sort by meta['weight'] by highest number (weighting) first
    matches.sort(key=lambda x: x.meta['weight'], reverse=True)

    yaracategories = YaraCategories(matches)
    print(yaracategories)


def matchtext(textdata: str) -> list:
    """
    """
    listofmatches = list()

    matches = rules.match(data=textdata, timeout=5)
    if len(matches) == 0:
        return None

    #Sort by meta['weight'] by highest number (weighting) first
    matches.sort(key=lambda x: x.meta['weight'], reverse=True)

    yaracategories = YaraCategories(matches)
    return yaracategories


def main(argv):
    for filename in argv:
        matchfile(filename)

if __name__ == '__main__':
    import sys

    if len(sys.argv) == 1:
        print('Usage: yarafiletype.py file [file ...]')
        print('Error: the following arguments are required: file')
        sys.exit(1)

    main(sys.argv[1:])
