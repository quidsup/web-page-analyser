#!/usr/bin/env python3
#Title       : Web Page Analyser
#Description :
#Author      : QuidsUp
#Date        : 2021-09-08
#Version     : 0.1
#Usage       : analyser.py webpage [webpage ...]

#Standard Imports
import sys

#Local Imports
from config import WebAnalyserConfig
from filedownloader import FileDownloader

config = WebAnalyserConfig()


def download_file(url):
    """
    """
    pageitems = list()
    categories_len = 0
    download_time = ''
    result_str = ''

    fdl = FileDownloader(url, config.validate_ssl)
    pageitems = fdl.files_downloaded

    if len(pageitems) == 0:
        print('Nothing Downloaded')
        return

    pageitems.sort(key=lambda x: x.created_time)           #Sort by downloaded time

    for item in pageitems:
        #download_time = item.download_time.strftime("%H:%M:%S.%f")[:-3]
        download_time = item.created_time.strftime('%H:%M:%S')
        categories_len = len(item.categories)
        if categories_len == 0:
            result_str = item.result
        elif categories_len == 1:
            result_str = format_categorystr(item.categories[0])

        print(f'{download_time} {item.datatype:<12} {result_str:<27} {item.url}')

        #Any additional categories to report?
        if categories_len > 1:
            for category in item.categories:
                #TODO sort out the column spacing to the left-hand side
                print(f'{format_categorystr(category)}')

    if len(fdl.files_downloaded) > 0:
        pageitems.extend(fdl.files_downloaded)


def format_categorystr(category: tuple) -> str:
    """
    Beautify the category column
    NOTE the inclusion of weighting scores will be reviewed at a later date
    """

    if category[1] != '':
        return f'{category[1]} - {category[0]}, {category[2]}'
    else:
        return f'{category[0]}, {category[2]}\n'


def main(argv):
    print('Web Page Analyser')

    #config.load()
    config.process_arguments(argv)

    for item in config.webpages:
        print(f'Analysing {item}')
        download_file(item)

        print()


if __name__ == '__main__':
   main(sys.argv)
