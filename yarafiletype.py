#!/usr/bin/env python3
#Title       : Yara File Type Matcher
#Description : Identify file based on magic number using Yara rules
#Author      : QuidsUp
#Date        : 2021-09-20
#Version     : 1.0
#Usage       : yarafiletype.py file [file ...]

#Standard Imports
try:
    import yara
except ModuleNotFoundError:
    print('Error: Missing yara module')
    print('Install with pip3 install yara-python')
    exit()

#Local Imports
import errorlogger

#Create logger
logger = errorlogger.logging.getLogger(__name__)

#Load list of rules from indexfiletype which links to files in ./rules/filetypes/*.yar
rules = yara.compile(filepath='indexfiletype.yar')


def matchbinarydata(binarydata: str) -> None:
    """
    Match against binarydata supplied from fileinfo object
    """
    matches = rules.match(data=binarydata, timeout=1)

    if len(matches) == 0:
        logger.info(f'No match for {binarydata[:32]}')
        return ''

    #Drop Text tag as we don't require it
    if matches[0].tags[0] == 'Text':
        return matches[0].rule
    #Nicely formatted return with tag name and rule name
    else:
        return f'{matches[0].tags[0]}/{matches[0].rule}'


def matchfile(filename: str) -> None:
    """
    Testing purposes to match against a supplied file
    """
    matches = rules.match(filename, timeout=1)

    if len(matches) == 0:
        print('No matches found')
        return

    for match in matches:
        print(f'{match.tags[0]}/{match.rule}')
        print(f'Strings: {match.strings}')
        print(f'Meta   : {match.meta}')


def main(argv):
    for filename in argv:
        matchfile(filename)

if __name__ == '__main__':
    import sys

    if len(sys.argv) == 1:
        print('Usage: yarafiletype.py file [file ...]')
        print('Error: the following arguments are required: file')
        sys.exit(1)

    main(sys.argv[1:])
