#!/usr/bin/env python3
#Title       : File Downloader and Analyser
#Description :
#Author      : QuidsUp
#Date        : 2021-09-08
#Version     : 0.1
#Usage       : N/A this module is loaded elsewhere

#Standard Imports
import errno
import requests

#Local Imports
import errorlogger
import yaraanalyser
import yarafiletype
from basicfileinfo import FileInfo
from ntrkhtmlparser import NTHTMLParser

#Create logger
logger = errorlogger.logging.getLogger(__name__)

file_extensions = {
    '.js' : 'JavaScript',
    '.vb' : 'VB Script',
    '.svg' : 'Image/SVG',
}


class FileDownloader:
    def __init__(self, url: str, validate_ssl: bool, **kwargs):
        """
        Parameters:
            url (str): Starting point to download
            validate_ssl (bool): Should SSL certs be validated? default False
        Optional Parameters:
            already_downloaded (set): Additional set of URLs which have been downloaded, in order to avoid duplication
            useragent (str): Custom user agent string

        """
        useragent = ''
        self._already_downloaded = set()
        self._files_downloaded = list()
        self._unixerrno = 0

        #Initiate requests session for multiple GETs to same domain
        self._session = requests.session()

        #Update already_downloaded set if it has been supplied
        self._already_downloaded.update(kwargs.get('already_downloaded', None) or {})

        #Set a custom user-agent to replace "Python Requests"
        useragent = kwargs.get('useragent', '')
        if useragent == '':
            useragent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36'
        self._session.headers.update({'User-Agent': useragent})

        #Optional disabling of SSL Verification, as self-signed certs are used in NoTrack
        if not validate_ssl:
            self._session.verify=False
            requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)

        self.__download_and_process(url)


    @property
    def already_downloaded(self):
        """
        Set of URLs which have been downloaded
        """
        return self._already_downloaded

    @property
    def files_downloaded(self):
        return self._files_downloaded

    @property
    def unixerrno(self):
        return self._unixerrno


    def __add_multiple_urls(self, newurls: set, knownresult=None) -> None:
        """
        Download multiple URLs
        """
        for newurl in newurls:
            self.__download_and_process(newurl, knownresult)


    def __analysefile(self, fileinfo: object):
        """
        Yara analysis of a file

        Parameters:
            fileinfo object
        Returns:
            list of tuples of the Yara matches found
        """
        yaraanalysis = yaraanalyser.matchtext(fileinfo.textdata)

        if yaraanalysis is None:                           #No Yara matches found
            return None

        #Download any urls which have been extracted from the Yara match
        self.__add_multiple_urls(yaraanalysis.urls, yaraanalysis.categories)

        return yaraanalysis.categories


    def __download_data(self, url: str) -> object:
        """
        Download
        """
        fileinfo = FileInfo()

        #Check the URL is valid and attempt correction
        u = requests.compat.urlparse(url)                  #Use urllib3 urlparse to validate the domain

        if u.scheme != '' and u.netloc != '':              #Valid http scheme and domain
            fileinfo.url = url
        elif u.scheme == '' and u.netloc != '':            #Invalid http scheme with valid domain
            #Correct the http scheme
            fileinfo.url = f'https://{u.netloc}{u.path}{u.query}'
            u = requests.compat.urlparse(fileinfo.url)
        elif u.scheme == '' and u.netloc == '' and u.path != '':
            fileinfo.url = f'https://{u.path}'
            u = requests.compat.urlparse(fileinfo.url)
        else:                                              #Invalid url
            return None

        fileinfo.parsed_url = u                            #Add URL to fileinfo object

        if fileinfo.url in self._already_downloaded:       #Prevent duplicate downloads
            return None


        #Attempt to GET data
        try:
            req = self._session.get(fileinfo.url, timeout=2)
        except requests.exceptions.SSLError as e:
            logger.warning(f'SSL Verification Error {e}')
            return None
        except requests.ConnectionError as e:
            logger.warning(f'Connection issue: {e}')
            return None
        except requests.Timeout as e:
            self._already_downloaded.add(fileinfo.url)
            self._unixerrno = errno.ENETUNREACH
            fileinfo.status_code = 408
            fileinfo.result = 'Timeout'
        except requests.RequestException as e:
            logger.error(e)
            return None
        except KeyboardInterrupt:
            return None
        else:
            fileinfo.result = self.__decode_http_result(req.status_code)
            self._already_downloaded.add(fileinfo.url)

            if fileinfo.result == 'Success':
                fileinfo.binarydata = req.content
                fileinfo.textdata = req.text

        return fileinfo


    def __check_tracking_pixel(self, sha256hash) -> bool:
        """
        Look for GIF Images with known sha256hash
        """
        if sha256hash == '6adc3d4c1056996e4e8b765a62604c78b1f867cceb3b15d0b9bedb7c4857f992':
            return True
        elif sha256hash == '548f2d6f4d0d820c6c5ffbeffcbd7f0e73193e2932eefe542accc84762deec87':
            #https://facebook.com/security/hsts-pixel.gif
            return True

        return False


    def __decode_http_result(self, status_code: int) -> str:
        """
        Validate download based on the HTML Status Code
        Set _unixerrno based on any status code which is considered a failure
        TODO Complete

        Parameters:
            status_code
        Returns:
            String representation
        """
        if status_code == 200:
            return 'Success'
        elif status_code == 403:
            self._unixerrno = errno.ECONNABORTED
            return 'Forbidden'
        if status_code == 404:
            self._unixerrno = errno.ENETUNREACH
            return 'Not Found'

        return ''


    def __download_and_process(self, url, knownresult=None):
        """
        """
        result = ''

        fileinfo = self.__download_data(url)

        #Something wrong with download but no error code
        if fileinfo == None:
            return

        #Something wrong with download, error code known
        if fileinfo.result != 'Success':
            self._files_downloaded.append(fileinfo)        #Add what we have
            return                                         #No point in continuing

        #Work out file type with Yara using the binarydata
        fileinfo.datatype = yarafiletype.matchbinarydata(fileinfo.binarydata)

        #Fallback to file extension if Yara rules failed to match datatype
        if fileinfo.datatype == '':
            fileinfo.datatype = self.__identify_text_file(fileinfo.file_ext)

        if fileinfo.datatype == 'HTML':
            result = self.__process_html(fileinfo)

            if result != '':
                fileinfo.result = result

        if not fileinfo.datatype.startswith('Image'):
            fileinfo.categories = self.__analysefile(fileinfo) or list()

        elif fileinfo.datatype == 'Image/GIF':
            if self.__check_tracking_pixel(fileinfo.sha256hash):
                fileinfo.categories.append(tuple(('Tracking Pixel', 'Generic', 3)))
            #else:
                #print(fileinfo.binarydata)
                #print(fileinfo.sha256hash)

        #Did we already know the result?
        if not fileinfo.categories and knownresult is not None:
            fileinfo.categories = knownresult

        self._files_downloaded.append(fileinfo)


    def __identify_text_file(self, file_ext: str) -> str:
        """
        Fallback option of using file extension if file type has not been determined with Yara
        Parameters:
            Lowercase file extension
        Returns:
            File type if found
            Blank string if not found
        """

        #Check if supplied file extension exists in the file_extensions dict
        if file_ext in file_extensions:
            return file_extensions[file_ext]

        #Not found, return blank string
        return ''


    def __process_html(self, fileinfo: object):
        """
        """
        parser = NTHTMLParser(fileinfo.url)
        parser.feed(fileinfo.textdata)

        parser.remoteimgs.sort()
        self.__add_multiple_urls(parser.remoteimgs)

        parser.remotescripts.sort()
        for item in parser.remotescripts:
            if 'polyfill' in item:
                logger.info(f'Ignoring Polyfill {item}')
                continue
            self.__download_and_process(item)


        return ''
